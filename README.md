# README #

Docker image definition for a Windows Server Core 2016 container running
Python 3 with ODBC Driver 13 for SQL Server for connecting to for example Azure
SQL Server instance. Main motivation for this was to enable seamless development,
testing, deployment cycle from Mac OS X (UnixODBC) to a Windows virtual machine
running Docker.
