
FROM python:3-windowsservercore-ltsc2016

SHELL [ "powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';" ]

# Install ODBC Driver 13 for SQL Server
RUN $odbc_url = ( 'https://download.microsoft.com/download/1/E/7/1E7B1181-3974-4B29-9A47-CC857B271AA2/English/X64/msodbcsql.msi' ); \
                Write-Host ( 'Downloading {0}...' -f $odbc_url ); \
                Invoke-WebRequest -Uri $odbc_url -Outfile 'odbc.msi'; \
                Write-Host 'Installing...'; \
                Start-Process msiexec.exe -Wait \
                -ArgumentList @( \
                                 '/I', \
                                 'odbc.msi', \
                                 '/qn', \
                                 'IACCEPTMSODBCSQLLICENSETERMS=YES', \
                                 'ALLUSERS=1' \
                               ); \
                Write-Host 'Verifying'; \
                Get-OdbcDriver; \
                Remove-Item 'odbc.msi' -Force; \
                Write-Host 'Done.';

CMD [ "python" ]

